package com.bignerdranch.android.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG = "QuizActivity.java";
    private static final String KEY_INDEX = "index";
    private static final String KEY_COUNTER = "counter";
    private static final String KEY_ANSWERED = "answered";
    private static final String KEY_IS_CHEATER = "isCheater";
    private static final String KEY_COUNT_OF_CHEAT = "count of cheats";
    private static final int REQUEST_CODE_CHEAT = 0;

    private Button mTrueButton;
    private Button mFalseButton;
    private Button mCheatButton;
    private ImageButton mNextButton;
    private ImageButton mPrevButton;
    private TextView mQuestionTextView;

    private Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_australia, true),
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_america, true),
            new Question(R.string.question_asia, true),
    };

    private int mCurrentIndex;
    private int mCorrectAnswerCount;
    private int mCountCheat = 3;
    private boolean mIsCheater;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState");
        outState.putInt(KEY_INDEX, mCurrentIndex);
        outState.putInt(KEY_COUNTER, mCorrectAnswerCount);
        outState.putBooleanArray(KEY_ANSWERED, saveAnswered());
        outState.putBoolean(KEY_IS_CHEATER, mIsCheater);
        outState.putInt(KEY_COUNT_OF_CHEAT, mCountCheat);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        if(savedInstanceState != null){
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mCorrectAnswerCount = savedInstanceState.getInt(KEY_COUNTER, 0);
            boolean[] answeredArray = savedInstanceState.getBooleanArray(KEY_ANSWERED);
            for (int i = 0; i < mQuestionBank.length; i++){
                if (answeredArray != null) {
                    mQuestionBank[i].setAnswered(answeredArray[i]);
                }
            }
            mIsCheater = savedInstanceState.getBoolean(KEY_IS_CHEATER, false);
            mCountCheat = savedInstanceState.getInt(KEY_COUNT_OF_CHEAT, 0);
        }

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);

        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                updateQuestion();
            }
        });

        mPrevButton = (ImageButton) findViewById(R.id.prev_button);
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCurrentIndex == 0) {
                    mCurrentIndex = 6;
                }
                mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
                updateQuestion();
            }
        });

        mCheatButton = (Button) findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
                Intent intent = CheatActivity.newIntent(QuizActivity.this,
                        answerIsTrue,
                        mCountCheat);
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

        mTrueButton = (Button) findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                checkAnswer(true);
            }
        });

        mFalseButton = (Button) findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(false);
            }
        });

        updateQuestion();

        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNextButton.callOnClick();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK){
            return;
        }
        if (requestCode == REQUEST_CODE_CHEAT){
            if (data == null){
                return;
            }
            mIsCheater = CheatActivity.wasAnswerShown(data);
            mCountCheat = CheatActivity.getCountOfCheats(data);
        }
    }

    private void updateQuestion() {
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
        if(!mQuestionBank[mCurrentIndex].isAnswered()){
        clickableButtons(true);
            mQuestionTextView.setTextColor(getResources().getColor(R.color.not_already_answered));
        } else {
            clickableButtons(false);
            mQuestionTextView.setTextColor(getResources().getColor(R.color.already_answered));
        }
    }

    private boolean[] saveAnswered(){
        boolean[] answeredArray = new boolean[mQuestionBank.length];
        for (int i = 0; i < mQuestionBank.length; i++){
            answeredArray[i] = mQuestionBank[i].isAnswered();
        }
        return answeredArray;
    }

    private boolean checkAllAnswered(){
        for (int i = 0; i < mQuestionBank.length; i++){
            if (!mQuestionBank[i].isAnswered()){
                return false;
            }
        }
        return true;
    }

    private void setAllNotAnswered(){
        for (int i = 0; i < mQuestionBank.length; i++){
            mQuestionBank[i].setAnswered(false);
        }
    }

    private void clickableButtons(boolean click){
        mTrueButton.setClickable(click);
        mFalseButton.setClickable(click);
    }

    private void checkAnswer(boolean userPressTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
        mQuestionTextView.setTextColor(getResources().getColor(R.color.already_answered));

        int messageResId = 0;

        if (mIsCheater){
            messageResId = R.string.judgment_toast;
        } else {
            if (userPressTrue == answerIsTrue){
                messageResId = R.string.correct_toast;
                mCorrectAnswerCount++;
            } else {
                messageResId = R.string.incorrect_toast;
            }
        }

        Toast toast = Toast.makeText(QuizActivity.this,
                messageResId,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 00, 300);
        toast.show();

        mQuestionBank[mCurrentIndex].setAnswered(true);

        clickableButtons(false);

        if(checkAllAnswered()){

            double percent = ((double) 100 / mQuestionBank.length) * mCorrectAnswerCount;

            toast = Toast.makeText(QuizActivity.this,
                    percent + "% of correct answer",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
            mCorrectAnswerCount = 0;
            setAllNotAnswered();
        }
    }
}
