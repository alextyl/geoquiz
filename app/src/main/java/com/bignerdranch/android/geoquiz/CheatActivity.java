package com.bignerdranch.android.geoquiz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends AppCompatActivity {

    private static final String EXTRA_ANSWER_IS_TRUE =
            "com.bignerdranch.android.geoquiz.answer_is_true";
    private static final String EXTRA_ANSWER_SHOWN =
            "com.bignerdranch.android.geoquiz.answer_shown";
    private static final String EXTRA_COUNT_OF_CHEAT = "count_of_cheat";
    private static final String KEY_ANSWER_SHOWN = "answer";
    private static final String KEY_ANSWER_IS_TRUE = "answerIsTrue";
    private static final String KEY_COUNT_OF_CHEAT = "count of cheats";

    private boolean mAnswerIsTrue;
    private boolean mIsAnswerShown;

    private TextView mAnswerTextView;
    private TextView mCheatsTextViwe;
    private Button mShowAnswerButton;

    private int mCountCheat;

    public static Intent newIntent(Context packageContext, boolean answerIsTrue, int countOfCheat){
        Intent intent = new Intent(packageContext, CheatActivity.class);
        intent.putExtra(EXTRA_ANSWER_IS_TRUE, answerIsTrue);
        intent.putExtra(EXTRA_COUNT_OF_CHEAT, countOfCheat);
        return intent;
    }

    public static boolean wasAnswerShown(Intent result){
        return result.getBooleanExtra(EXTRA_ANSWER_SHOWN, false);
    }

    public static int getCountOfCheats(Intent count){
        return count.getIntExtra(EXTRA_COUNT_OF_CHEAT, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_ANSWER_SHOWN, mIsAnswerShown);
        outState.putBoolean(KEY_ANSWER_IS_TRUE, mAnswerIsTrue);
        outState.putInt(KEY_COUNT_OF_CHEAT, mCountCheat);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        if(savedInstanceState != null){
            mCountCheat = savedInstanceState.getInt(KEY_COUNT_OF_CHEAT, 0);
            if(savedInstanceState.getBoolean(KEY_ANSWER_SHOWN, false)){
                setAnswerShownResult(true);
                mIsAnswerShown = true;
                mShowAnswerButton = (Button) findViewById(R.id.show_answer_button);
                mShowAnswerButton.setVisibility(View.INVISIBLE);
                mAnswerTextView = (TextView) findViewById(R.id.answer_text_view);
                if(savedInstanceState.getBoolean(KEY_ANSWER_IS_TRUE, true)){
                    mAnswerTextView.setText(R.string.true_button);
                } else {
                    mAnswerTextView.setText(R.string.false_button);
                }
            }
        } else {
            mCountCheat = getIntent().getIntExtra(EXTRA_COUNT_OF_CHEAT, 0);
        }

        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);


        mCheatsTextViwe = (TextView) findViewById(R.id.api_version_text);
        mCheatsTextViwe.setText("Cheat\'s count: " + mCountCheat);

        mAnswerTextView = (TextView) findViewById(R.id.answer_text_view);
        mShowAnswerButton = (Button) findViewById(R.id.show_answer_button);

        if(mCountCheat <= 0){
            mShowAnswerButton.setVisibility(View.INVISIBLE);
        } else {
            mShowAnswerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCheatsTextViwe.setText("Cheat\'s count: " + (--mCountCheat));
                    if(mAnswerIsTrue){
                        mAnswerTextView.setText(R.string.true_button);
                    } else {
                        mAnswerTextView.setText(R.string.false_button);
                    }
                    setAnswerShownResult(true);
                    mIsAnswerShown = true;
                    makeAnimation(view);
                }
            });
        }
    }

    private void setAnswerShownResult(boolean isAnswerShown){
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        data.putExtra(EXTRA_COUNT_OF_CHEAT, mCountCheat);
        setResult(RESULT_OK, data);
    }

    private void makeAnimation(final View view){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Animator anim = ViewAnimationUtils.createCircularReveal(view,
                    view.getWidth() / 2,
                    view.getHeight() / 2,
                    view.getWidth(),
                    0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }
}
